lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "na.suzume",
      scalaVersion := "2.12.4"
    )),
    name := "scalatest-example"
  )


libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.0.5" % Test,
  "com.lihaoyi" %% "fastparse" % "1.0.0"
)

