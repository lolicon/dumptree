case class TreeNode[T](data: T, children: Seq[TreeNode[T]] = Nil)

object TreeDump {
  def prefix = "+-"

  def tailIndent = "  "

  def headIndent = "| "

  def asciiDisplay(node: TreeNode[String], indent: String = tailIndent, trailing: Boolean = false): Seq[String] = {
    s"$prefix${node.data}" +: (node.children match {
      case Nil => if (trailing) "" :: Nil else Nil
      case only :: Nil => asciiDisplay(only, tailIndent, true).map(indent + _)
      case head :+ tail => head.flatMap(asciiDisplay(_, headIndent, false).map(indent + _)) ++ asciiDisplay(tail, headIndent, true).map(indent + _)
    })
  }
}
