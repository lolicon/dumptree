
class TreeDumpTest extends org.scalatest.FunSuite {
  test("dumptree") {
    val content = TreeDump.asciiDisplay(TreeNode("Root",
      children = List(
        TreeNode("level1-1", children = TreeNode("level2-1", children = TreeNode("level3-1") :: Nil) :: Nil),
        TreeNode("level1-2"),
        TreeNode("level1-3")))).mkString("\n")
    val expected = "+-Root\n  +-level1-1\n  | +-level2-1\n  |   +-level3-1\n  |   \n  +-level1-2\n  +-level1-3\n  "
    assert(content.trim == expected.trim)
  }
}
